﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testWpfReportScript
{
  public class dbData
    {
        public static List<testScript> TesNumber { get; set; } 

        public static List<testScript> getScript()
        {
            var ListScripts = new List<testScript>();
            for (int i = 0; i < 10; i++)
            {
                var scr = new testScript() { ID = i, Description = $"Description {i}" };
                ListScripts.Add(scr);
            }

            return ListScripts;
        }
    }

   
}
