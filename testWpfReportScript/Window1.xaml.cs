﻿using DevExpress.XtraPrinting.Caching;
using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


namespace testWpfReportScript
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();

            GenerateRepxReport("reportScript.repx");

        }


        private void GenerateRepxReport(string reportName)
        {
            // _reportPreview.Visibility = Visibility.Collapsed;
            //XtraReport report = new XtraReport();
            //report.LoadLayout(FindRepxReport(reportName));
            //_reportPreview.DocumentSource = report;
            //report.CreateDocument();

            var storage = new MemoryDocumentStorage();
            var rep = new XtraReport();
            rep.LoadLayout(FindRepxReport(reportName));
            var cachedReportSource = new CachedReportSource(rep, storage);
            _reportPreview.DocumentSource = cachedReportSource;
            cachedReportSource.CreateDocumentAsync();
            _reportPreview.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// Return the Repx Report Full Path
        /// </summary>
        public static string FindRepxReport(string reportName)
        {
            string ReportPath = "";
            try
            {
                string directoryPath = Environment.CurrentDirectory;
                ReportPath = System.IO.Path.Combine(directoryPath, @"Reports\");
                ReportPath = System.IO.Path.Combine(ReportPath, reportName);

            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show("Raporti nuk Gjendet :" + ex.Message);

            };

            return ReportPath;
        }


    }
}
