﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace testWpfReportScript
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
           InitializeComponent();
            dbData.TesNumber = dbData.getScript();
            var rep = new XtraReport();
            rep.LoadLayout(Window1.FindRepxReport("reportScript.repx"));
            _reportDesigner.OpenDocument(rep);
        }

        private void SimpleButton_Click(object sender, RoutedEventArgs e)
        {
           
            var x = new Window1();
            x.Show();
        }
    }
}
